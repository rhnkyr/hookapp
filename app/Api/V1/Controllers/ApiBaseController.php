<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 13.02.2018
 * Time: 20:59
 */

namespace App\Api\V1\Controllers;

use App\Events\ActiveChecker;
use App\Http\Controllers\Controller;
use Auth;

class ApiBaseController extends Controller
{
    public $user;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        $user       = Auth::guard()->user();
        $this->user = $user;

        event(new ActiveChecker($user));

        //\DB::enableQueryLog();
    }
}