<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 13.02.2018
 * Time: 20:58
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\GoalRequest;
use App\Enums\MediaType;
use App\Helper\CustomHelper;
use App\Models\Goal;
use App\Models\GoalMedia;
use App\Models\UserAchievementsTracker;
use App\Models\UserClap;
use App\Models\UserVote;
use Exception;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Video;
use FFMpeg\Coordinate\TimeCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class GoalController extends ApiBaseController
{

    /**
     * User Goal list
     * @return JsonResponse
     */
    public function goalList(): JsonResponse
    {
        return CustomHelper::toJson($this->user->goals);
    }

    /**
     * Create goal with initial media if exist
     * @param GoalRequest $request
     * @return JsonResponse
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws FileNotFoundException
     */
    public function create(GoalRequest $request): JsonResponse
    {

        $uid = $this->user->uid;
        $gid = (string)Uuid::uuid4();

        $uat = UserAchievementsTracker::where('uid', $uid)->first();

        if ($uat === null) {
            return CustomHelper::notFound();
        }

        //Check user goal media and create if exist
        if ($request['goal_media'] !== null && !empty($request['goal_media'])) {

            $file = $request->file('goal_media');
            $name = $gid . '.' . $file->getClientOriginalExtension();

            /*$destination = public_path('uploads/' . $uid);
            if (!$file->move($destination, $name)) {
                throw new HttpException(500);
            }*/

            if (!\Storage::disk('public_uploads')->put($uid . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

            if ((int)trim($request['media_type']) === MediaType::Photo) {
                try {
                    $file = $uid . DIRECTORY_SEPARATOR . $name;
                    $path = \Storage::disk('public_uploads')->path($file);
                    $img  = Image::make($path);
                    $img->fit(getenv('IMAGE_THUMB'))->save($path, 80);

                } catch (FileNotFoundException $e) {
                    throw new HttpException(500);
                }
            }

            if ((int)trim($request['media_type']) === MediaType::Video) {

                $vgid = (string)Uuid::uuid4();

                $vfile = $uid . DIRECTORY_SEPARATOR . $name;
                $path  = \Storage::disk('public_uploads')->path($vfile);

                $ffmpeg = FFMpeg::create([
                    'ffmpeg.binaries'  => storage_path('app/ffmpeg/ffmpeg'),
                    'ffprobe.binaries' => storage_path('app/ffmpeg/ffprobe'),
                    'timeout'          => 3600, // The timeout for the underlying process
                    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                ]);

                $video = $ffmpeg->open($path);

                $url = \Storage::disk('public_uploads')->path($uid . DIRECTORY_SEPARATOR . $vgid . '.jpg');

                $video->frame(TimeCode::fromSeconds(2))
                    ->save($url);

                $thumb = $uid . DIRECTORY_SEPARATOR . $vgid . '.jpg';
                $tpath = \Storage::disk('public_uploads')->path($thumb);
                $img   = Image::make($tpath);
                $img->fit(getenv('IMAGE_THUMB'))->save($tpath, 80);

                $format = new Video\X264();
                $format->setAudioCodec('libmp3lame');

                $url = \Storage::disk('public_uploads')->path($uid . DIRECTORY_SEPARATOR . $vgid . '.mp4');

                $video->save($format, $url);

                \Storage::disk('public_uploads')->delete($vfile);

                $name  = $vgid . '.mp4';
                $thumb = $vgid . '.jpg';
            }


            $gmid = (string)Uuid::uuid4();

            $goal_media              = new GoalMedia();
            $goal_media->gmid        = $gmid;
            $goal_media->gid         = $gid;
            $goal_media->media_url   = $name;
            $goal_media->media_type  = trim($request['media_type']);
            $goal_media->video_thumb = $thumb ?? null;
            $goal_media->is_active   = 1;

            if (!$goal_media->save()) {
                throw new HttpException(500);
            }

            ++$uat->story_content;
        }

        //Create goal
        $goal              = new Goal();
        $goal->gid         = $gid;
        $goal->uid         = $uid;
        $goal->title       = trim($request['title']);
        $goal->investment  = trim($request['investment']);
        $goal->dead_line   = strtotime(trim($request['dead_line']));
        $goal->vote_finish = null;
        $goal->status      = 1;

        if (!$goal->save()) {
            throw new HttpException(500);
        }

        ++$uat->create_goal;

        $uat->total_investment += (int)trim($request['investment']);

        if (!$uat->save()) {
            throw new HttpException(500);
        }

        return CustomHelper::statusOk();

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(Request $request): JsonResponse
    {

        $goal = Goal::where('gid', trim($request['gid']))->first();
        if ($goal === null) {
            return CustomHelper::notFound();
        }

        if (!$goal->delete()) {
            throw new HttpException(500);
        }

        return CustomHelper::statusOk();
    }

    /**
     * Gaol detail
     * @param null $gid
     * @return JsonResponse
     */
    public function detail($gid = null): JsonResponse
    {
        if ($gid === null) {
            return CustomHelper::notFound();
        }

        //$goal = Goal::where([['gid', '=', $gid], ['uid', '=', $this->user->uid]])->first();
        //todo : reward join i yapılacak coin tablosu ile
        $goal = Goal::where('gid', trim($gid))->first();
        if ($goal === null) {
            return CustomHelper::notFound();
        }

        $goal->reward      = 50;
        $goal->media_count = $goal->media()->count();

        return CustomHelper::toJson($goal);

    }

    /**
     * Increments clap count
     * @param $gid
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function clap($gid): JsonResponse
    {
        try {

            $gid = trim($gid);

            $uid = $this->user->uid;
            //Check user claps count
            $user_claps = UserClap::where([['gid', '=', $gid], ['uid', '=', $uid]])->count();
            if ($user_claps === env('MAX_CLAPS')) {
                return CustomHelper::maxClap();
            }

            //$goal = Goal::where('gid', $gid)->increment('claps');

            $goal = Goal::with('user')->where('gid', $gid)->first();
            if ($goal === null) {
                return CustomHelper::notFound();
            }

            //Add a clap to user and goail
            $user_clap      = new UserClap();
            $user_clap->gid = $gid;
            $user_clap->uid = $uid;
            $user_clap->save();

            $uat = UserAchievementsTracker::where('uid', $uid)->first();

            if ($uat === null) {
                return CustomHelper::notFound();
            }

            ++$uat->receive_clap;
            ++$uat->give_clap;

            if (!$uat->save()) {
                throw new HttpException(500);
            }

            ++$goal->claps;
            $goal->save();

            return CustomHelper::statusOk();
        } catch (\Exception $ex) {
            throw new HttpException(500);
        }
    }

    /**
     * Increments up vote count
     * @param $gid
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function upVote($gid): JsonResponse
    {
        try {
            Goal::where('gid', $gid)->increment('up_votes');

            $user_clap      = new UserVote();
            $user_clap->gid = $gid;
            $user_clap->uid = $this->user->uid;
            $user_clap->save();

            return CustomHelper::statusOk();
        } catch (\Exception $ex) {
            throw new HttpException(500);
        }
    }

    /**
     * Increments down vote count
     * @param $gid
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function downVote($gid): JsonResponse
    {
        try {
            Goal::where('gid', $gid)->increment('down_votes');

            $user_clap      = new UserVote();
            $user_clap->gid = $gid;
            $user_clap->uid = $this->user->uid;
            $user_clap->save();

            return CustomHelper::statusOk();
        } catch (\Exception $ex) {
            throw new HttpException(500);
        }
    }

    /**
     * List of completed goals by page
     * @param $page
     * @return JsonResponse
     */
    public function completedGoals($page): JsonResponse
    {

        //\DB::enableQueryLog();
        //TODO : burası düzenlenecek
        $perPage = (int)getenv('PAGER');
        $goals   = Goal::with(['media', 'user'])
            //->where([['status', '=', GoalStatus::Done], ['dead_line', '<', time()]])
            ->whereNotIn('goals.gid', \DB::table('user_votes')->where('uid', $this->user->uid)->pluck('gid'))
            ->orderBy('dead_line', 'desc')
            ->skip($perPage * trim($page))
            ->take($perPage)
            ->get();

        //dd(\DB::getQueryLog());

        return CustomHelper::toJson($goals);

    }

}