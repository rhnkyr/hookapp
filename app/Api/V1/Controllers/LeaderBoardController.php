<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 13.02.2018
 * Time: 20:58
 */

namespace App\Api\V1\Controllers;

use App\Helper\CustomHelper;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class LeaderBoardController extends ApiBaseController
{


    /**
     * Weekly leader board list
     * @return \Illuminate\Http\JsonResponse
     */
    public function weekly() : JsonResponse
    {

        $info = \Cache::remember(md5(time()), getenv('REDIS_FIVE'), function () {

            $list = User::join('points', 'points.uid', '=', 'users.uid', 'left')
                ->where('users.role', '<>', 1)
                ->whereRaw('WEEK(`claim_date`, 3) = ' . date('W'))
                ->groupBy('users.uid')
                ->orderBy('total_points', 'desc')
                ->get(['users.uid', 'users.profile_picture', 'users.username', 'users.multiplier', \DB::raw('sum(points.amount) as total_points')]);

            $arr = $list->toArray();

            $ret              = CustomHelper::searchForId($this->user->uid, $arr);
            $a                = new \stdClass();
            $a->list          = \array_slice($arr, 0, 50);
            $a->current_user  = $this->user;
            $a->user_position = $ret === null ? -1 : $ret + 1;

            return $a;
        });

        return CustomHelper::toJson($info);
    }

    /**
     * Monthly leader board list
     * @return JsonResponse
     */
    public function monthly() : JsonResponse
    {
        $info = \Cache::remember(md5(time()), getenv('REDIS_FIVE'), function () {

            $list = User::join('points', 'points.uid', '=', 'users.uid', 'left')
                ->where('users.role', '<>', 1)
                ->whereRaw(' MONTH(claim_date) = MONTH(CURRENT_DATE()) AND YEAR(claim_date) = YEAR(CURRENT_DATE())')
                ->groupBy('users.uid')
                ->orderBy('total_points', 'desc')
                ->get(['users.uid', 'users.profile_picture', 'users.username', 'users.multiplier', \DB::raw('sum(points.amount) as total_points')]);

            $arr = $list->toArray();

            $ret              = CustomHelper::searchForId($this->user->uid, $arr);
            $a                = new \stdClass();
            $a->list          = \array_slice($arr, 0, 50);
            $a->current_user  = $this->user;
            $a->user_position = $ret === null ? -1 : $ret + 1;

            return $a;
        });

        return CustomHelper::toJson($info);
    }

    /**
     * All time leader board list
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        $info = \Cache::remember(md5(time()), getenv('REDIS_FIVE'), function () {

            $list = User::join('points', 'points.uid', '=', 'users.uid', 'left')
                ->where('users.role', '<>', 1)
                ->groupBy('users.uid')
                ->orderBy('total_points', 'desc')
                ->get(['users.uid', 'users.profile_picture', 'users.username', \DB::raw('sum(points.amount) as total_points')]);

            $arr = $list->toArray();

            $ret              = CustomHelper::searchForId($this->user->uid, $arr);
            $a                = new \stdClass();
            $a->list          = \array_slice($arr, 0, 50);
            $a->current_user  = $this->user;
            $a->user_position = $ret === null ? -1 : $ret + 1;

            return $a;
        });

        return CustomHelper::toJson($info);
    }

}