<?php

namespace App\Api\V1\Controllers;

use App\Helper\CustomHelper;
use Auth;
use Illuminate\Http\JsonResponse;

class LogoutController extends ApiBaseController
{
    /**
     * Log the user out (Invalidate the token)
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        Auth::guard()->logout();

        return CustomHelper::toJson(['message' => 'Successfully logged out']);
    }
}
