<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 13.02.2018
 * Time: 20:58
 */

namespace App\Api\V1\Controllers;

use App\Enums\MediaType;
use App\Helper\CustomHelper;
use App\Models\Goal;
use App\Models\GoalMedia;
use App\Models\UserAchievementsTracker;
use Exception;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MediaController extends ApiBaseController
{

    /**
     * Goal media list
     * @param $gid
     * @return JsonResponse
     */
    public function goalMediaList($gid): JsonResponse
    {

        $goal = Goal::where('gid', $gid)->first();
        if ($goal === null) {
            return CustomHelper::notFound();
        }

        return CustomHelper::toJson($goal->media);
    }

    /**
     * Create goal with initial media if exist
     * @param Request $request
     * @return JsonResponse
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function create(Request $request): JsonResponse
    {
        $uid  = $this->user->uid;
        $gid  = trim($request['gid']);
        $gmid = (string)Uuid::uuid4();
        $goal = Goal::where('gid', $gid)->first();
        if ($goal === null) {
            return CustomHelper::notFound();
        }

        //Check goal media and create if exist
        if (trim($request['goal_media']) !== null) {


            $file = $request->file('goal_media');
            $name = $gid . '-' . time() . '.' . $file->getClientOriginalExtension();

            /*$destination = public_path('uploads/' . $uid);
            if (!$file->move($destination, $name)) {
                throw new HttpException(500);
            }*/

            if (!\Storage::disk('public_uploads')->put($uid . DIRECTORY_SEPARATOR . $name, \File::get($file))) {
                throw new HttpException(500);
            }

            if (trim($request['media_type']) === MediaType::Photo) {
                try {
                    $file = $uid . DIRECTORY_SEPARATOR . $name;
                    $path = \Storage::disk('public_uploads')->path($file);
                    $img  = Image::make($path);
                    $img->fit(getenv('IMAGE_THUMB'))->save($path, 80);

                } catch (FileNotFoundException $e) {
                    throw new HttpException(500);
                }
            }

            if ((int)trim($request['media_type']) === MediaType::Video) {

                $vgid = (string)Uuid::uuid4();

                $vfile = $uid . DIRECTORY_SEPARATOR . $name;
                $path  = \Storage::disk('public_uploads')->path($vfile);

                $ffmpeg = FFMpeg::create([
                    'ffmpeg.binaries'  => storage_path('app/ffmpeg/ffmpeg'),
                    'ffprobe.binaries' => storage_path('app/ffmpeg/ffprobe'),
                    'timeout'          => 3600, // The timeout for the underlying process
                    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                ]);

                $video = $ffmpeg->open($path);

                $url = \Storage::disk('public_uploads')->path($uid . DIRECTORY_SEPARATOR . $vgid . '.jpg');

                $video->frame(TimeCode::fromSeconds(2))
                    ->save($url);

                $thumb = $uid . DIRECTORY_SEPARATOR . $vgid . '.jpg';
                $tpath = \Storage::disk('public_uploads')->path($thumb);
                $img   = Image::make($tpath);
                $img->fit(getenv('IMAGE_THUMB'))->save($tpath, 80);

                $format = new Video\X264();
                $format->setAudioCodec('libmp3lame');

                $url = \Storage::disk('public_uploads')->path($uid . DIRECTORY_SEPARATOR . $vgid . '.mp4');

                $video->save($format, $url);

                \Storage::disk('public_uploads')->delete($vfile);

                $name  = $vgid . '.mp4';
                $thumb = $vgid . '.jpg';
            }

            $goal_media              = new GoalMedia();
            $goal_media->gmid        = $gmid;
            $goal_media->gid         = $gid;
            $goal_media->media_url   = $name;
            $goal_media->media_type  = trim($request['media_type']);
            $goal_media->video_thumb = $thumb ?? null;
            $goal_media->is_active   = 1;

            if (!$goal_media->save()) {
                throw new HttpException(500);
            }

            $uat = UserAchievementsTracker::where('uid', $uid)->first();

            if ($uat === null) {
                return CustomHelper::notFound();
            }

            ++$uat->story_content;

            if (!$uat->save()) {
                throw new HttpException(500);
            }
        }

        return CustomHelper::statusOk();

    }

    /**
     * Delete goal media by id
     * @param $gmid
     * @return JsonResponse
     * @throws Exception
     */
    public function delete($gmid): JsonResponse
    {

        $gmid = trim($gmid);

        $goal_media = GoalMedia::where('gmid', $gmid)->first();
        if ($goal_media === null) {
            return CustomHelper::notFound();
        }

        if (!$goal_media->delete()) {
            throw new HttpException(500);
        }

        $uat = UserAchievementsTracker::where('uid', $this->user->id)->first();

        if ($uat === null) {
            return CustomHelper::notFound();
        }

        --$uat->story_content;

        if (!$uat->save()) {
            throw new HttpException(500);
        }

        return CustomHelper::statusOk();
    }

}