<?php

namespace App\Api\V1\Controllers;

use App\Enums\PromotionType;
use App\Enums\ReadStatus;
use App\Helper\CustomHelper;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserNotification;
use App\Models\UserPromotion;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Config;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NotificationController extends ApiBaseController
{
    public function nonReadNotifications()
    {
        $un = UserNotification::user($this->user->uid)
            ->read(0)
            ->latest()
            ->get();

        return CustomHelper::toJson($un);
    }

    public function readNotifications()
    {
        $un = UserNotification::user($this->user->uid)
            ->read(1)
            ->latest()
            ->get();

        return CustomHelper::toJson($un);
    }

    public function seen()
    {
        $uns = UserNotification::user($this->user->uid)->read(0)->latest()->get();

        foreach ($uns as $un) {
            $un->is_read = ReadStatus::Read;
            $un->save();
        }

        return CustomHelper::statusOk();
    }
}
