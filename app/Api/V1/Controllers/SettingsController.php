<?php

namespace App\Api\V1\Controllers;

use App\Enums\PromotionType;
use App\Helper\CustomHelper;
use App\Models\Achievement;
use App\Models\Promotion;
use App\Models\UserPromotion;
use Carbon\Carbon;
use Config;
use App\Models\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SettingsController extends Controller
{
    /**
     * Achievements list
     * @return JsonResponse
     */
    public function achievements() : JsonResponse
    {

        if (!\Cache::has(md5('hook-achievement'))) {
            $a = Achievement::first();
            \Cache::forever(md5('hook-achievement'), $a);
        }

        $a = \Cache::get(md5('hook-achievement'));

        return CustomHelper::toJson($a);
    }

    //todo : bu kısım yeni sisteme göre düzenlecek
    public function promotions()
    {

        if (!\Cache::has(md5('hook-promotions'))) {
            $promotions = Promotion::orderBy('multiplier')->get();
            \Cache::forever(md5('hook-promotions'), $promotions);
        }

        $promotions = \Cache::get(md5('hook-promotions'));

        return CustomHelper::toJson($promotions);
    }
}
