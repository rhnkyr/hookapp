<?php

namespace App\Api\V1\Controllers;

use App\Enums\PromotionType;
use App\Enums\ReadStatus;
use App\Helper\CustomHelper;
use App\Models\Reward;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserNotification;
use App\Models\UserPromotion;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Config;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    /**
     * User sign up
     * @param SignUpRequest $request
     * @param JWTAuth $JWTAuth
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth): JsonResponse
    {
        $req = $request->all();


        if (isset($req['ref_code'])) {
            $check_code = User::where('ref_code', trim($req['ref_code']))->first();

            if ($check_code !== null) {
                $uat = UserAchievementsTracker::where('uid', $check_code->uid)->first();

                if ($uat === null) {
                    return CustomHelper::notFound();
                }

                ++$uat->invite_x_friends;

                if (!$uat->save()) {
                    throw new HttpException(500);
                }
            } else {
                return CustomHelper::refCodeError();
            }
        } else {
            return CustomHelper::refCodeError();
        }

        $uid = (string)Uuid::uuid4();

        //Check profile picture and create if exist
        if (isset($request['profile_picture']) && !empty($request['profile_picture'])) {

            $file                 = $request->file('profile_picture');
            $profile_picture_name = $uid . '-' . time() . '.' . $file->getClientOriginalExtension();

            if (!\Storage::disk('public_uploads')->put($uid . DIRECTORY_SEPARATOR . $profile_picture_name, file_get_contents($file))) {
                throw new HttpException(500);
            }

            $file = $uid . DIRECTORY_SEPARATOR . $profile_picture_name;
            $path = \Storage::disk('public_uploads')->path($file);
            $img  = Image::make($path);
            $img->fit(getenv('IMAGE_THUMB'))->save($path, 80);

        }

        $user                  = new User();
        $user->uid             = $uid;
        $user->profile_picture = $profile_picture_name ?? null;
        $user->username        = trim($req['username']);
        $user->password        = trim($req['password']);
        $user->email           = trim($req['email']);
        $user->one_signal_id   = isset($req['one_signal_id']) ? trim($req['one_signal_id']) : null;
        $user->os              = trim($req['os']);
        $user->multiplier      = 0;
        $user->is_active       = 1;
        $user->ref_code        = CustomHelper::generateRandomString(6);

        if (!$user->save()) {
            throw new HttpException(500);
        }

        //Create user initial promotion
        $promotion             = new UserPromotion();
        $promotion->uid        = $uid;
        $promotion->pid        = PromotionType::Beginner;
        $promotion->notified   = 1;
        $promotion->claim_date = Carbon::now();

        if (!$promotion->save()) {
            throw new HttpException(500);
        }

        //user quest tracker default data
        $qt      = new UserQuestTracker();
        $qt->uid = $uid;

        if (!$qt->save()) {
            throw new HttpException(500);
        }

        //user achievement default data
        $a      = new UserAchievement();
        $a->uid = $uid;

        if (!$a->save()) {
            throw new HttpException(500);
        }

        //user achievement tracker default data
        $at      = new UserAchievementsTracker();
        $at->uid = $uid;

        if (!$at->save()) {
            throw new HttpException(500);
        }

        //Give 100 coin
        $reward         = new Reward();
        $reward->uid    = $uid;
        $reward->gid    = null;
        $reward->amount = 100;

        if (!$reward->save()) {
            throw new HttpException(500);
        }

        //check if user provide ref_code

        if (isset($req['one_signal_id'])) {
            CustomHelper::pushNotification(trim($req['one_signal_id']),
                'Welcome to Hook. Watch this, if you want to learn how you can accomplish your goals with us.',
                'http://www.google.com');

            $n          = new UserNotification();
            $n->uid     = $uid;
            $n->url     = 'http://www.google.com';
            $n->text    = 'Welcome to Hook. Watch this, if you want to learn how you can accomplish your goals with us.';
            $n->is_read = ReadStatus::NonRead;
            $n->save();
        }


        /*if (!Config::get('boilerplate.sign_up.release_token')) {
            return CustomHelper::statusOk();
        }*/

        $token = $JWTAuth->fromUser($user);

        return response()->json([
            'status' => 'ok',
            'token'  => $token
        ], 201);
    }

    /**
     * Check user name for registration
     * @param $username
     * @return JsonResponse
     */
    public function checkUsername($username): JsonResponse
    {

        $check = User::where('username', strtolower(trim($username)))->first();

        if ($check !== null) {
            return CustomHelper::userExist();
        }

        return CustomHelper::statusOk();

    }

    /**
     * Check email for registration
     * @param $email
     * @return JsonResponse
     */
    public function checkEmail($email): JsonResponse
    {

        $check = User::where('email', strtolower(trim($email)))->first();

        if ($check !== null) {
            return CustomHelper::emailExist();
        }

        return CustomHelper::statusOk();

    }

    /**
     * Check ref code for registration
     * @param $ref_code
     * @return JsonResponse
     */
    public function checkRefCode($ref_code): JsonResponse
    {

        $check_code = User::where('ref_code', trim($ref_code))->first();

        if ($check_code !== null) {
            return CustomHelper::statusOk();
        }
        return CustomHelper::refCodeError();

    }
}
