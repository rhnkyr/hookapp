<?php

namespace App\Api\V1\Controllers;


use App\Helper\CustomHelper;
use App\Models\FormData;
use App\Models\Subscription;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Subscription
     * @param Request $request
     * @return JsonResponse
     */
    public function subscription(Request $request): JsonResponse
    {

        $check = Subscription::where('email', $request->input('email'))->first();

        if ($check === null) {
            $sub        = new Subscription();
            $sub->email = $request->input('email');
            $sub->save();
        }
        return CustomHelper::statusOk();
    }


    /**
     * Site contact form data save
     * @param Request $request
     * @return JsonResponse
     */
    public function contact(Request $request): JsonResponse
    {

        $data          = new FormData();
        $data->email   = $request->input('email');
        $data->message = $request->input('message');

        $data->save();
        return CustomHelper::statusOk();

    }

}
