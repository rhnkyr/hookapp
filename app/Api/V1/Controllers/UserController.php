<?php

namespace App\Api\V1\Controllers;

use App\Enums\PointType;
use App\Enums\ReadStatus;
use App\Helper\CustomHelper;
use App\Models\Goal;
use App\Models\Point;
use App\Models\Promotion;
use App\Models\Reward;
use App\Models\User;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserNotification;
use App\Models\UserQuestTracker;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Intervention\Image\Facades\Image;

class UserController extends ApiBaseController
{

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(): JsonResponse
    {

        $uid = $this->user->uid;
        //todo redis süresi 0 dan 5 e çekilecek
        $info = \Cache::remember(md5('me-' . $uid), getenv('REDIS_FIVE'), function () use ($uid) {

            $info                  = $this->user;
            $info->profile_picture = $this->user->profile_image_path;
            $info->total_points    = Point::where('uid', $uid)->sum('amount');
            $info->total_coins     = Reward::where('uid', $uid)->sum('amount');
            $info->goal_created    = Goal::where('uid', $uid)->count();
            $info->goal_done       = Goal::where('uid', $uid)->where('status', '<>', 1)->count();
            $info->total_claps     = Point::where('uid', $uid)->where('claim_type', PointType::GiveClap)->count();
            $info->total_clapped   = Point::where('uid', $uid)->where('claim_type', PointType::ReceiveClap)->count();
            $info->total_rewards   = Reward::where('uid', $uid)->sum('amount');
            $info->tracker         = UserAchievementsTracker::where('uid', $uid)->first();

            return $info;

        });

        return CustomHelper::toJson($info);

    }

    /**
     * User information by user id
     * @param $uid
     * @return JsonResponse
     */
    public function user($uid): JsonResponse
    {

        $info = \Cache::remember(md5('user-' . $uid), getenv('REDIS_FIVE'), function () use ($uid) {

            $user = User::where('uid', $uid)->first();
            if ($user === null) {
                CustomHelper::notFound();
            }
            $info                  = $user;
            $info->profile_picture = $user->profile_image_path;
            $info->total_points    = Point::where('uid', $uid)->sum('amount');
            $info->total_coins     = Reward::where('uid', $uid)->sum('amount');
            $info->goal_created    = Goal::where('uid', $uid)->count();
            $info->goal_done       = Goal::where('uid', $uid)->where('status', '<>', 1)->count();
            $info->total_claps     = Point::where('uid', $uid)->where('claim_type', PointType::GiveClap)->count();
            $info->total_clapped   = Point::where('uid', $uid)->where('claim_type', PointType::ReceiveClap)->count();
            $info->total_rewards   = Reward::where('uid', $uid)->sum('amount');

            return $info;

        });

        return CustomHelper::toJson($info);

    }

    /**
     * Uer profile edit
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(Request $request): JsonResponse
    {
        $uid  = $this->user->uid;
        $user = User::where('uid', $uid)->first();

        if ($user === null) {
            return CustomHelper::notFound();
        }

        $req = $request->all();

        if ($request['profile_picture'] !== null && !empty($request['profile_picture'])) {

            $file                 = $request->file('profile_picture');
            $profile_picture_name = $user->uid . '-' . time() . '.' . $file->getClientOriginalExtension();
            if (!\Storage::disk('public_uploads')->put($user->uid . DIRECTORY_SEPARATOR . $profile_picture_name, file_get_contents($file))) {
                throw new HttpException(500);
            }

            $file = $user->uid . DIRECTORY_SEPARATOR . $profile_picture_name;
            $path = \Storage::disk('public_uploads')->path($file);
            $img  = Image::make($path);
            $img->fit(getenv('IMAGE_THUMB'))->save($path, 80);

        }

        if (isset($req['username'])) {
            $user->username = trim($req['username']);
        }
        if (isset($req['profile_picture'])) {
            $user->profile_picture = $profile_picture_name;
        }
        if (isset($req['email'])) {
            $user->email = trim($req['email']);
        }
        if (isset($req['password'])) {
            $user->password = trim($req['password']);
        }

        $user->save();

        if (isset($req['profile_complete']) && null !== trim($req['profile_complete'])) {
            $q = UserQuestTracker::where('uid', $user->uid)->first();
            if ($q !== null && $q->complete_profile_notified === 0) {
                CustomHelper::pushNotification($user->one_signal_id, 'User profile done', 'app/profile');
                $q->complete_profile          = 1;
                $q->complete_profile_notified = 1;
                $q->save();

                $n          = new UserNotification();
                $n->uid     = $uid;
                $n->url     = 'app/profile';
                $n->text    = 'User profile done';
                $n->is_read = ReadStatus::NonRead;
                $n->save();
            }
        }

        return CustomHelper::toJson($user);

    }

    /**
     * User achievements
     * @return JsonResponse
     */
    public function achievements(): JsonResponse
    {
        $uid = $this->user->uid;
        $a   = \Cache::remember(md5('achievements-' . $uid), getenv('REDIS_FIVE'), function () use ($uid) {
            return UserAchievement::where('uid', $uid)->first();
        });

        return CustomHelper::toJson($a);
    }

    /**
     * Promotions
     * @return JsonResponse
     */
    public function promotions(): JsonResponse
    {

        if (!\Cache::has(md5('hook-promotions'))) {
            $promotions = Promotion::orderBy('multiplier')->get();
            \Cache::forever(md5('hook-promotions'), $promotions);
        }

        $promotions = \Cache::get(md5('hook-promotions'));

        return CustomHelper::toJson($promotions);
    }

    /**
     * User claim button event
     * @param Request $request
     * @return JsonResponse
     */
    public function claim(Request $request): JsonResponse
    {
        $uid = $this->user->uid;

        $ua = UserAchievement::where('uid', $uid)->first();
        if ($ua === null) {
            return CustomHelper::notFound();
        }
        $ua->{$request['what']} = 0;
        $ua->save();

        return CustomHelper::statusOk();


    }


}
