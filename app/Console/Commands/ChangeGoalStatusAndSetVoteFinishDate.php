<?php

namespace App\Console\Commands;

use App\Enums\GoalStatus;
use App\Models\Goal;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeGoalStatusAndSetVoteFinishDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goal:change-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Goal status and vote date when dead-line is reached';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $goals = Goal::where('dead_line', '<', Carbon::now())->get();

        foreach ($goals as $goal) {
            $goal->status      = GoalStatus::Done;
            $goal->vote_finish = date('Y-m-d 00:00:00', strtotime(env('GOAL_VOTE_PERIOD')));
            $goal->save();
        }
        
        $this->info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
