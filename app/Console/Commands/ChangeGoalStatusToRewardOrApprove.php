<?php

namespace App\Console\Commands;

use App\Enums\GoalStatus;
use App\Helper\CustomHelper;
use App\Models\Goal;
use App\Models\Reward;
use App\Models\UserAchievementsTracker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ChangeGoalStatusToRewardOrApprove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goal:change-reward';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Goal status and reward or approve vote-finish is reached';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {
        $goals = Goal::whereNotNull('vote_finish')->where('vote_finish', '<', Carbon::now())->where('status', GoalStatus::Done)->get();

        if ($goals->count() > 0) {
            foreach ($goals as $goal) {

                $up   = $goal->up_votes;
                $down = $goal->down_votes;
                $sum  = $up + $down;

                if ($sum !== 0) {

                    $diff = (($up / $sum) * 100) - (($down / $sum) * 100);
                    $diff = round($diff);

                    if ($diff < 0) {
                        continue;
                    }

                    if ($diff > (int)env('GOAL_PERCENT_DIFF')) {
                        $goal->status = GoalStatus::Rewarded;
                        $goal->save();

                        $r = CustomHelper::reward($diff, $goal->investment, $goal->claps);


                        $reward         = new Reward();
                        $reward->uid    = $goal->uid;
                        $reward->gid    = $goal->gid;
                        $reward->amount = round($r, 1);

                        $reward->save();

                        $uat = UserAchievementsTracker::where('uid', $goal->uid)->first();

                        CustomHelper::pushNotification($goal->user->one_signal_id, 'Great work! You accomplished your goal.Here is your reward: ' . round($r, 1) . '.', 'app/goals');

                        if ($uat === null) {
                            return CustomHelper::notFound();
                        }

                        ++$uat->accomplish_goal;
                        //$uat->total_reward += $goal->investment;

                        if (!$uat->save()) {
                            throw new HttpException(500);
                        }

                    } else {
                        $goal->status = GoalStatus::OnApprove;
                        $goal->save();
                    }
                }
            }
        }

        $this->info('ChangeGoalStatusToRewardOrApprove process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
