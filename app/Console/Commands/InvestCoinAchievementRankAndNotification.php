<?php

namespace App\Console\Commands;

use App\Enums\PointType;
use App\Helper\CustomHelper;
use App\Models\Achievement;
use App\Models\Point;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;

//Todo : Console\Kernel içinden çağırılıyor
class InvestCoinAchievementRankAndNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:invest-coin-achievement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invest Coin Achievements handler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {

        if (!\Cache::has(md5('hook-achievements'))) {
            $a = Achievement::first();
            \Cache::forever(md5('hook-achievements'), $a);
        }

        $a  = \Cache::get(md5('hook-achievements'));
        $rc = explode('|', $a->invest_more_than_x_coin);

        //$uas = UserAchievement::all();

        UserAchievement::chunk(500, function ($uas) use ($rc) {
            foreach ($uas as $ua) {

                $jd = json_decode($ua->invest_more_than_x_coin_rank_notified);

                if ($jd->rank4 !== 1) {
                    $uat = UserAchievementsTracker::where('uid', $ua->uid)->first();
                    if ($jd->rank1 === 0 && ($uat->invest_more_than_x_coin >= $rc[0] && $uat->invest_more_than_x_coin <= $rc[1])) {
                        CustomHelper::pushNotification($ua->user->one_signal_id, env('GENERAL'),'app/achievements');
                        $ua->invest_more_than_x_coin_rank          = 1;
                        $ua->invest_more_than_x_coin_rank_claim          = 1;
                        $ua->invest_more_than_x_coin_rank_notified = '{"rank1": 1,"rank2": 0,"rank3": 0,"rank4": 0}';
                        $ua->save();

                        $point             = new Point();
                        $point->uid        = $ua->user->uid;
                        $point->amount     = env('RANK_1') + floor((int)env('RANK_1') * ($ua->user->multiplier / 100));
                        $point->claim_type = PointType::Invest;
                        $point->claim_date = Carbon::now();

                        if (!$point->save()) {
                            throw new HttpException(500);
                        }

                        $track = UserAchievementsTracker::where('uid', $ua->user->uid)->first();
                        ++$track->accomplishment;

                        if (!$track->save()) {
                            throw new HttpException(500);
                        }

                    } elseif ($jd->rank2 === 0 && ($uat->invest_more_than_x_coin >= $rc[1] && $uat->invest_more_than_x_coin <= $rc[2])) {
                        CustomHelper::pushNotification($ua->user->one_signal_id, env('GENERAL'),'app/achievements');
                        $ua->invest_more_than_x_coin_rank          = 2;
                        $ua->invest_more_than_x_coin_rank_claim          = 1;
                        $ua->invest_more_than_x_coin_rank_notified = '{"rank1": 1,"rank2": 1,"rank3": 0,"rank4": 0}';
                        $ua->save();

                        $point             = new Point();
                        $point->uid        = $ua->user->uid;
                        $point->amount     = env('RANK_2') + floor((int)env('RANK_1') * ($ua->user->multiplier / 100));
                        $point->claim_type = PointType::Invest;
                        $point->claim_date = Carbon::now();

                        if (!$point->save()) {
                            throw new HttpException(500);
                        }

                        $track = UserAchievementsTracker::where('uid', $ua->user->uid)->first();
                        ++$track->accomplishment;

                        if (!$track->save()) {
                            throw new HttpException(500);
                        }

                    } elseif ($jd->rank3 === 0 && ($uat->invest_more_than_x_coin >= $rc[2] && $uat->invest_more_than_x_coin <= $rc[3])) {
                        CustomHelper::pushNotification($ua->user->one_signal_id, env('GENERAL'),'app/achievements');
                        $ua->invest_more_than_x_coin_rank          = 3;
                        $ua->invest_more_than_x_coin_rank_claim          = 1;
                        $ua->invest_more_than_x_coin_rank_notified = '{"rank1": 1,"rank2": 1,"rank3": 1,"rank4": 0}';
                        $ua->save();

                        $point             = new Point();
                        $point->uid        = $ua->user->uid;
                        $point->amount     = env('RANK_3') + floor((int)env('RANK_1') * ($ua->user->multiplier / 100));
                        $point->claim_type = PointType::Invest;
                        $point->claim_date = Carbon::now();

                        if (!$point->save()) {
                            throw new HttpException(500);
                        }

                        $track = UserAchievementsTracker::where('uid', $ua->user->uid)->first();
                        ++$track->accomplishment;

                        if (!$track->save()) {
                            throw new HttpException(500);
                        }

                    } elseif ($jd->rank4 === 0 && $uat->invest_more_than_x_coin >= $rc[3]) {
                        CustomHelper::pushNotification($ua->user->one_signal_id, env('GENERAL'),'app/achievements');
                        $ua->invest_more_than_x_coin_rank          = 4;
                        $ua->invest_more_than_x_coin_rank_claim          = 1;
                        $ua->invest_more_than_x_coin_rank_notified = '{"rank1": 1,"rank2": 1,"rank3": 1,"rank4": 1}';
                        $ua->save();

                        $point             = new Point();
                        $point->uid        = $ua->user->uid;
                        $point->amount     = env('RANK_4') + floor((int)env('RANK_1') * ($ua->user->multiplier / 100));
                        $point->claim_type = PointType::Invest;
                        $point->claim_date = Carbon::now();

                        if (!$point->save()) {
                            throw new HttpException(500);
                        }

                        $track = UserAchievementsTracker::where('uid', $ua->user->uid)->first();
                        ++$track->accomplishment;

                        if (!$track->save()) {
                            throw new HttpException(500);
                        }

                    } else {
                        continue;
                    }
                }

            }
        });


        $this->info('InvestCoinAchievementRankAndNotification process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
