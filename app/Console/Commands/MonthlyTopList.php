<?php

namespace App\Console\Commands;

use App\Enums\PointType;
use App\Helper\CustomHelper;
use App\Models\Point;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MonthlyTopList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:monthly-top-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Controls monthly top list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {

        $points = Point::whereRaw(' MONTH(claim_date) = MONTH(CURRENT_DATE()) AND YEAR(claim_date) = YEAR(CURRENT_DATE())')
            ->groupBy('uid')
            ->orderBy('total', 'desc')
            ->limit(10)
            ->get(['uid', \DB::raw('sum(amount) as total')]);

        $i = 0;
        foreach ($points as $point) {
            if ($i === 0) { //1.

                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_1');
                $p->claim_type = PointType::Monthly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Monthly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->monthly_top_1_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'monthly top 1','app/leader');
                    $q->monthly_top_1          = 1;
                    $q->monthly_top_1_notified = 1;
                    $q->save();
                }


            } elseif ($i === 2) {//3.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_3');
                $p->claim_type = PointType::Monthly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Monthly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->monthly_top_3_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'monthly top 3','app/leader');
                    $q->monthly_top_3          = 1;
                    $q->monthly_top_3_notified = 1;
                    $q->save();
                }

            } elseif ($i === 4) {//5.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_5');
                $p->claim_type = PointType::Monthly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Monthly_Point Hata');
                    throw new HttpException(500);
                }


                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->monthly_top_5_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'monthly top 5','app/leader');
                    $q->monthly_top_5          = 1;
                    $q->monthly_top_5_notified = 1;
                    $q->save();
                }

            } elseif ($i === 9) {//10.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_10');
                $p->claim_type = PointType::Monthly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Monthly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->monthly_top_10_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'monthly top 10','app/leader');
                    $q->monthly_top_10          = 1;
                    $q->monthly_top_10_notified = 1;
                    $q->save();
                }

            }
            $i++;
        }

        $this->info('MonthlyTopList process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
