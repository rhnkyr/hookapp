<?php

namespace App\Console\Commands;

use App\Enums\PointType;
use App\Enums\PromotionType;
use App\Enums\ReadStatus;
use App\Helper\CustomHelper;
use App\Models\Achievement;
use App\Models\Point;
use App\Models\Promotion;
use App\Models\User;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserNotification;
use App\Models\UserPromotion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;

//Todo : Console\Kernel içinden çağırılıyor
class PointChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:point-checker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User point check handler for badges';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {

        if (!\Cache::has(md5('hook-promotions'))) {
            $p = Promotion::get();
            \Cache::forever(md5('hook-promotions'), $p);
        }

        $p = \Cache::get(md5('hook-promotions'));

        User::chunk(500, function ($users) use ($p) {
            foreach ($users as $user) {
                $user_points    = Point::where('uid', $user->uid)->sum('amount');
                $user_promotion = $user->promotions()->latest()->first();

                if ($user_points > 500 && $user_points < 2500) {
                    if ($user_promotion->pid !== PromotionType::Expert && $user_promotion->notified === 0) {

                        $vars = [
                            '#1#' => $user_points,
                            '#2#' => PromotionType::getDescription(PromotionType::Expert),
                            '#3#' => $p[1]->multiplier,
                        ];

                        $message = strtr(env('BADGE'), $vars);

                        CustomHelper::pushNotification($user->one_signal_id, $message, 'app/promotions');

                        $this->PromoteUser($user->uid, PromotionType::Expert);

                        $user->multiplier = $p[1]->multiplier;
                        $user->save();

                        $track = UserAchievementsTracker::where('uid', $user->uid)->first();
                        ++$track->badge_claim;

                        if (!$track->save()) {
                            throw new HttpException(500);
                        }

                        $n          = new UserNotification();
                        $n->uid     = $user->uid;
                        $n->url     = 'app/promotions';
                        $n->text    = $message;
                        $n->is_read = ReadStatus::NonRead;
                        $n->save();

                    }
                } elseif ($user_points > 2500 && $user->multiplier < 15) {
                    if ($user_promotion->pid !== PromotionType::Master && $user_promotion->notified === 0) {

                        $vars = [
                            '#1#' => $user_points,
                            '#2#' => PromotionType::getDescription(PromotionType::Master),
                            '#3#' => $p[2]->multiplier,
                        ];

                        $message = strtr(env('BADGE'), $vars);

                        CustomHelper::pushNotification($user->one_signal_id, $message, 'app/promotions');

                        $this->PromoteUser($user->uid, PromotionType::Master);

                        $user->multiplier = $p[2]->multiplier;
                        $user->save();

                        $n          = new UserNotification();
                        $n->uid     = $user->uid;
                        $n->url     = 'app/promotions';
                        $n->text    = $message;
                        $n->is_read = ReadStatus::NonRead;
                        $n->save();

                        \Log::info('Promoted : ' . PromotionType::getDescription(2));
                    }
                } /*elseif ($user_points > 499999 && $user_points < 999999) {
            if ($user_promotion->pid !== PromotionType::Star && $user_promotion->notified === 0) {
                CustomHelper::pushNotification($user->one_signal_id,'');

                $this->PromoteUser($user->uid, PromotionType::Star);

                $user->multiplier = PromotionType::Star * 10;
                $user->save();

                \Log::info('Promoted : ' . PromotionType::getDescription(3));
            }
        } elseif ($user_points > 999999) {
            if ($user_promotion->pid !== PromotionType::SuperStar && $user_promotion->notified === 0) {
                CustomHelper::pushNotification($user->one_signal_id,'');

                $this->PromoteUser($user->uid, PromotionType::SuperStar);

                $user->multiplier = PromotionType::SuperStar * 10;
                $user->save();

                \Log::info('Promoted : ' . PromotionType::getDescription(4));
            }
        }*/

            }
        });


        $this->info('PointChecker process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }

    /**
     * User promote setter
     * @param $uid
     * @param $promotionType
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    private function PromoteUser($uid, $promotionType)
    {
        $promotion             = new UserPromotion();
        $promotion->uid        = $uid;
        $promotion->pid        = $promotionType;
        $promotion->notified   = 1;
        $promotion->claim_date = Carbon::now();

        if (!$promotion->save()) {
            throw new HttpException(500);
        }
    }
}
