<?php

namespace App\Console\Commands;

use App\Enums\PointType;
use App\Helper\CustomHelper;
use App\Models\Achievement;
use App\Models\Goal;
use App\Models\GoalMedia;
use App\Models\Point;
use App\Models\UserAchievement;
use App\Models\UserAchievementsTracker;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;


class WeeklyGoalMediaCheckAndNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:weekly-goal-media-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly goal media check handler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {
        $gms = GoalMedia::whereRaw('created_at < NOW() - INTERVAL 1 WEEK')->get();

        if ($gms->count() > 0) {
            foreach ($gms as $gm) {

                $osid = $gm->goal->user->one_signal_id;
                CustomHelper::pushNotification($osid, 'We didn’t get any update for your goal this week. Let us know your process', 'app/goals');

            }
        }

        $this->info('WeeklyGoalMediaCheckAndNotification process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
