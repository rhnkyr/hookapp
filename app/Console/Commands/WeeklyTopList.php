<?php

namespace App\Console\Commands;

use App\Enums\PointType;
use App\Helper\CustomHelper;
use App\Models\Point;
use App\Models\UserQuestTracker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Exception\HttpException;

class WeeklyTopList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:weekly-top-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Controls weekly top list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle()
    {
        $points = Point::whereRaw('WEEK(`claim_date`, 3) = ' . date('W'))
            ->groupBy('uid')
            ->orderBy('total', 'desc')
            ->limit(10)
            ->get(['uid', \DB::raw('sum(amount) as total')]);

        $i = 0;
        foreach ($points as $point) {
            if ($i === 0) { //1.

                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_1');
                $p->claim_type = PointType::Weekly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Weekly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->weekly_top_1_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'weekly top 1','app/leader');
                    $q->weekly_top_1          = 1;
                    $q->weekly_top_1_notified = 1;
                    $q->save();
                }


            } elseif ($i === 2) {//3.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_3');
                $p->claim_type = PointType::Weekly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Weekly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->weekly_top_3_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'weekly top 3','app/leader');
                    $q->weekly_top_3          = 1;
                    $q->weekly_top_3_notified = 1;
                    $q->save();
                }

            } elseif ($i === 4) {//5.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_5');
                $p->claim_type = PointType::Weekly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Weekly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();

                if ($q !== null && $q->weekly_top_5_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'weekly top 5','app/leader');
                    $q->weekly_top_5          = 1;
                    $q->weekly_top_5_notified = 1;
                    $q->save();
                }

            } elseif ($i === 9) {//10.
                $p             = new Point();
                $p->uid        = $point->uid;
                $p->amount     = env('TOP_10');
                $p->claim_type = PointType::Weekly;
                $p->claim_date = Carbon::now();

                if (!$p->save()) {
                    \Log::error('Weekly_Point Hata');
                    throw new HttpException(500);
                }

                $q = UserQuestTracker::where('uid', $point->uid)->first();
                if ($q !== null && $q->weekly_top_10_notified === 0) {
                    CustomHelper::pushNotification($point->user->one_signal_id, 'weekly top 10','app/leader');
                    $q->weekly_top_10          = 1;
                    $q->weekly_top_10_notified = 1;
                    $q->save();
                }

            }
            $i++;
        }

        $this->info('WeeklyTopList process done successfully!');
        //\Log::info('ChangeGoalStatusAndSetVoteFinishDate process done successfully!');
        return true;
    }
}
