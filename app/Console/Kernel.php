<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('user:receive-clap-achievement')->everyMinute();
        $schedule->command('user:give-clap-achievement')->everyMinute();
        $schedule->command('user:create-goal-achievement')->everyMinute();
        $schedule->command('user:accomplish-goal-achievement')->everyMinute();
        $schedule->command('user:total-investment-achievement')->everyMinute();
        $schedule->command('user:total-reward-achievement')->everyMinute();
        $schedule->command('user:story-content-achievement')->everyMinute();
        $schedule->command('user:invite-friends-achievement')->everyMinute();
        $schedule->command('user:invest-coin-achievement')->everyMinute();
        $schedule->command('user:accomplishment-achievement')->everyMinute();
        $schedule->command('user:badge-claim-achievement')->everyMinute();
        $schedule->command('user:point-checker')->everyMinute();

        //Every 3 days
        //$schedule->command('user:weekly-goal-media-check')->cron('0 0 */3 * *');
        $schedule->command('user:weekly-goal-media-check')->sundays()->at('23:00');

        $schedule->command('goal:change-status')->everyFiveMinutes();

        $schedule->command('user:weekly-top-list')->sundays()->at('23:59');
        $schedule->command('user:monthly-top-list')->daily()->at('23:59')->when(function () {
            return Carbon::now()->day === Carbon::now()->endOfMonth()->day;
        });
        $schedule->command('user:yearly-top-list')->daily()->at('23:59')->when(function () {
            return Carbon::now()->day === Carbon::now()->endOfYear()->day;
        });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
    }
}
