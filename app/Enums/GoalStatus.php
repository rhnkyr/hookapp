<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class GoalStatus extends Enum
{
    const OnProgress = 1;
    const OnApprove = 2;
    const Done = 3;
    const Rewarded = 4;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::OnProgress:
                return 'On Progress';
                break;
            case self::OnApprove:
                return 'On Approve';
                break;
            case self::Done:
                return 'Done';
                break;
            case self::Rewarded:
                return 'Rewarded';
                break;
            default:
                return self::getKey($value);
        }
    }
}
