<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class MediaType extends Enum
{
    const Photo = 1;
    const Video = 2;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::Photo:
                return 'Photo';
            case self::Video:
                return 'Video';
                break;
            default:
                return self::getKey($value);
        }
    }
}
