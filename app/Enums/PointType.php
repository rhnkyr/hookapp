<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PointType extends Enum
{
    const CreateGoal = 1;
    const AccomplishGoal = 2;
    const GiveClap = 3;
    const ReceiveClap = 4;
    const ProcessContent = 5;
    const Invite = 6;
    const Weekly = 7;
    const Monthly = 8;
    const Yearly = 9;
    const Invest = 10;
    const Media = 11;
    const TotalInvest = 12;
    const TotalReward = 13;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::CreateGoal:
                return 'Goal Creation Point';
                break;
            case self::AccomplishGoal:
                return 'Goal Accomplishment Point';
                break;
            case self::GiveClap:
                return 'Give Clap Point';
                break;
            case self::ReceiveClap:
                return 'Receive Clap Point';
                break;
            case self::ProcessContent:
                return 'Process Content Point';
                break;
            case self::Invite:
                return 'Invite Point';
                break;
            case self::Weekly:
                return 'Weekly Top Point';
                break;
            case self::Monthly:
                return 'Monthly Top Point';
                break;
            case self::Yearly:
                return 'Yearly Top Point';
                break;
            case self::Invest:
                return 'Invest Point';
                break;
            case self::TotalInvest:
                return 'Total Invest Point';
                break;
            case self::Media:
                return 'Goal Media';
                break;
            case self::TotalReward:
                return 'Goal Media';
                break;
            default:
                return self::getKey($value);
        }
    }
}
