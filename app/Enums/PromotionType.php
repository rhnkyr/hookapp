<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PromotionType extends Enum
{
    const Beginner = 0;
    const Expert = 1;
    const Master = 2;
    const Star = 3;
    const SuperStar = 4;

    /**
     * Get the description for an enum value
     *
     * @param  int $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::Beginner:
                return 'Beginner';
                break;
            case self::Expert:
                return 'Expert';
                break;
            case self::Master:
                return 'Master';
                break;
            case self::Star:
                return 'Star';
                break;
            case self::SuperStar:
                return 'SuperStar';
                break;
            default:
                return self::getKey($value);
        }
    }
}
