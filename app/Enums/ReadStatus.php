<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ReadStatus extends Enum
{
    const NonRead = 0;
    const Read = 1;

    /**
     * Get the description for an enum value
     *
     * @param  int  $value
     * @return string
     */
    public static function getDescription(int $value): string
    {
        switch ($value) {
            case self::NonRead:
                return 'Non Read';
                case self::Read:
                return 'Read';
            break;
            default:
                return self::getKey($value);
        }
    }
}
