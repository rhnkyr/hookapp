<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 13.02.2018
 * Time: 22:45
 */

namespace App\Helper;


use Illuminate\Http\JsonResponse;
use Intervention\Image\Facades\Image;

class CustomHelper
{

    /**
     * Username exist
     * @return \Illuminate\Http\JsonResponse
     */
    public static function userExist(): JsonResponse
    {
        return response()->json([
            'status'  => 'error',
            'message' => 'User name already exits'
        ], 202);
    }

    /**
     * Email exist
     * @return \Illuminate\Http\JsonResponse
     */
    public static function emailExist(): JsonResponse
    {
        return response()->json([
            'status'  => 'error',
            'message' => 'Email already exits'
        ], 202);
    }

    /**
     * Ref code error
     * @return \Illuminate\Http\JsonResponse
     */
    public static function refCodeError(): JsonResponse
    {
        return response()->json([
            'status'  => 'error',
            'message' => 'Reference code is wrong'
        ], 202);
    }


    /**
     * Json status ok
     * @return \Illuminate\Http\JsonResponse
     */
    public static function statusOk(): JsonResponse
    {
        return response()->json([
            'status' => 'ok'
        ], 201);
    }

    /**
     * Json not found
     * @return \Illuminate\Http\JsonResponse
     */
    public static function notFound(): JsonResponse
    {
        return response()->json([
            'status' => 'not found'
        ], 404);
    }

    /**
     * Max clap reached
     * @return \Illuminate\Http\JsonResponse
     */
    public static function maxClap(): JsonResponse
    {
        return response()->json([
            'status' => 'max clap reached'
        ], 401);
    }

    /**
     * To Json helper
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function toJson($data): JsonResponse
    {
        return response()->json($data);
    }

    /**
     * Push notification handler
     * @param $userId
     * @param $message
     * @param string $url
     * @param null $data
     */
    public static function pushNotification($userId, $message, $url = null, $data = null)
    {
        if ($userId !== NULL) {
            \OneSignal::sendNotificationToUser($message, $userId, $url, $data, $buttons = null, $schedule = null);
        }
    }

    /**
     * Upload base64 image
     * @param $base64_image
     * @param $id
     * @return bool|string
     */
    public static function uploadImage($base64_image, $id)
    {

        try {
            $base64_str = substr($base64_image, strpos($base64_image->input('base64_image'), ',') + 1);
            //decode base64 string
            $image   = base64_decode($base64_str);
            $png_url = $id . '-' . time() . '.jpg';
            $path    = storage_path('app/public/images/' . $png_url);
            $path2   = storage_path('app/public/images/thumb_' . $png_url);

            file_put_contents($path, $image);

            $img = Image::make($path);

            $img->resize(getenv('IMAGE_BIG'), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path, 80);

            //thumbnail
            $img->fit(getenv('IMAGE_THUMB'))->save($path2, 80);

            return $png_url;

        } catch (\Exception $exception) {
            \Log::error($exception->getFile() . '-' . $exception->getMessage());
        }

        return false;
    }

    /**
     * Array key finder
     * @param $id
     * @param $array
     * @return int|null|string
     */
    public static function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            if ($val['uid'] === $id) {
                return $key;
            }
        }
        return null;
    }

    /**
     * Generates random string with numeric values
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function reward($diff, $investment, $claps)
    {

        $a = 0;

        if ($investment <= 50) {
            $a = $investment * 0.5;
        } else if ($investment > 50 && $investment <= 250) {
            $a = $investment * 0.4;
        } else if ($investment > 250 && $investment <= 500) {
            $a = $investment * 0.3;
        } else if ($investment > 500 && $investment <= 1000) {
            $a = $investment * 0.2;
        } else if ($investment > 1000 && $investment <= 2500) {
            $a = $investment * 0.1;
        } else if ($investment > 2500) {
            $a = $investment * 0.05;
        }

        $c = 1;

        if ($claps > 100 && $claps <= 500) {
            $c = 1.1;
        } else if ($claps > 500 && $claps <= 1000) {
            $c = 1.2;
        } else if ($claps > 1000 && $claps <= 5000) {
            $c = 1.3;
        } else if ($claps > 5000 && $claps <= 10000) {
            $c = 1.4;
        } else if ($claps > 10000) {
            $c = 1.5;
        }

        return round($diff / 100,1) * $a * $c;
    }
}