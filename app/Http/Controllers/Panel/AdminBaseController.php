<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 11.02.2018
 * Time: 20:21
 */

namespace App\Http\Controllers\Panel;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AdminBaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:web');
        if (!\Auth::check()) {
            redirect()->route('index');
        }

        \View::share('upload_path', \Config::get('filesystems.disks.public_uploads.path'));
    }

}