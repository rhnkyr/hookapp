<?php

namespace App\Http\Controllers\Panel;

use App\Helper\CustomHelper;
use App\Models\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    //
    public function index()
    {
        //todo : remove
        /*try {
            //CustomHelper::pushNotification('fef6ab71-36a5-4cc1-b082-e06745833537', 'kakamela kakayım sana bir kilo boya', 'app/achievements');
            CustomHelper::pushNotification('fd779a1f-9c97-4868-b22a-65a0cd4781ab',
                'Welcome to Hook. Watch this, if you want to learn how you can accomplish your goals with us.',
                'app/achievements');

            $n          = new UserNotification();
            $n->uid     = 'f8fc81e5-3c27-4233-8b88-ba9e01dd0925';
            $n->text    = 'Welcome to Hook. Watch this, if you want to learn how you can accomplish your goals with us.';
            $n->is_read = 0;
            $n->save();

        }catch (\Exception $ex){
            \Log::error('Hata : ', (array)$ex);
        }*/


        return view('auth.login');
    }

    public function check(Request $request)
    {
        if (\Auth::guard('web')->attempt(['username' => $request['username'], 'password' => $request['password']])) {
            return redirect()->route('admin.welcome');
        }
        return back()->with('error', '');
    }

    public function logout()
    {
        \Auth::guard('web')->logout();
        return redirect()->route('index');
    }

}
