<?php

namespace App\Http\Controllers\Panel;

use App\Models\Goal;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

class GoalController extends AdminBaseController
{
    //
    public function goalList(Request $request)
    {

        $all     = Goal::join('goal_media', 'goals.gid', '=', 'goal_media.gid', 'left')
            ->orderBy('created_at', 'desc')
            ->get(['goals.*', \DB::raw('count(goal_media.id) as total_media')]);
        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );


        return view('admin.goals.list', ['goals' => $data]);
    }

}
