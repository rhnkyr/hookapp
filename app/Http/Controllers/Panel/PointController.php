<?php

namespace App\Http\Controllers\Panel;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

class PointController extends AdminBaseController
{
    //

    public function weekList(Request $request)
    {

        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();

        $title = 'Weekly List (' . date('d-m', strtotime($monday)) . ' / ' . date('d-m', strtotime($sunday)) . ')';

        $all = User::join('points', 'points.uid', '=', 'users.uid')
            ->where('users.role', '<>', 1)
            ->whereRaw('WEEK(`claim_date`, 3) = ' . date('W'))
            ->groupBy('users.uid')
            ->orderBy('total_points', 'desc')
            ->get(['users.uid', 'users.profile_picture', 'users.username', \DB::raw('sum(points.amount) as total_points')]);

        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );


        return view('admin.points.list', ['points' => $data, 'title' => $title]);
    }

    public function monthList(Request $request)
    {
        $month = 'Monthly List (' . date('m-Y') . ')';

        $all = User::join('points', 'points.uid', '=', 'users.uid')
            ->where('users.role', '<>', 1)
            ->whereRaw(' MONTH(claim_date) = MONTH(CURRENT_DATE()) AND YEAR(claim_date) = YEAR(CURRENT_DATE())')
            ->groupBy('users.uid')
            ->orderBy('total_points', 'desc')
            ->get(['users.uid', 'users.profile_picture', 'users.username', \DB::raw('sum(points.amount) as total_points')]);

        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );


        return view('admin.points.list', ['points' => $data, 'title' => $month]);
    }

    public function allTimeList(Request $request)
    {
        $all = User::join('points', 'points.uid', '=', 'users.uid')
            ->where('users.role', '<>', 1)
            ->groupBy('users.uid')
            ->orderBy('total_points', 'desc')
            ->get(['users.uid', 'users.profile_picture', 'users.username', \DB::raw('sum(points.amount) as total_points')]);

        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );


        return view('admin.points.list', ['points' => $data, 'title' => 'All Time']);
    }

}
