<?php

namespace App\Http\Controllers\Panel;

use App\Models\Promotion;

class PromotionController extends AdminBaseController
{
    //
    public function promotionList()
    {
        $promotions = Promotion::get();
        return view('admin.promotions.list', ['promotions' => $promotions]);
    }

    public function promotion($pid)
    {
        return view('admin.welcome');
    }

    public function save()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

}
