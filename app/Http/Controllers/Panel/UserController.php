<?php

namespace App\Http\Controllers\Panel;

use App\Models\{
    Coin, Goal, GoalMedia, Point, User
};
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

class UserController extends AdminBaseController
{
    //
    public function userList(Request $request)
    {
        $all = User::join('goals', 'goals.uid', '=', 'users.uid', 'left')
            ->where('users.role', '<>', 1)
            ->groupBy('users.uid')
            ->orderBy('total_goals', 'desc')
            ->get(['users.uid', 'users.profile_picture', 'users.username', 'users.os', 'users.email', 'users.multiplier',
                'users.created_at', 'users.is_active', \DB::raw('count(goals.id) as total_goals')]);

        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );

        return view('admin.users.list', ['users' => $data]);
    }

    public function goalList(Request $request)
    {
        $all = Goal::join('goal_media', 'goals.gid', '=', 'goal_media.gid', 'left')
            ->where('uid', $request['uid'])
            ->orderBy('created_at', 'desc')
            ->get(['goals.*', \DB::raw('count(goal_media.id) as total_media')]);

        if ($all->count() === 0) {
            return redirect()->back();
        }

        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );

        return view('admin.users.goal-list', ['goals' => $data]);
    }

    public function goalMediaList($gid)
    {
        $medias = GoalMedia::with('goal')->where('gid', $gid)->orderBy('created_at', 'desc')->get();

        if ($medias->count() === 0) {
            return redirect()->back();
        }
        return view('admin.users.goal-media-list', ['medias' => $medias]);
    }

    public function coinList(Request $request)
    {
        $all     = Coin::where('uid', $request['uid'])->orderBy('created_at', 'desc')->get();
        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        if ($all->count() === 0) {
            return redirect()->back();
        }

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );

        return view('admin.users.coin-list', ['coins' => $data]);
    }

    public function pointList(Request $request)
    {
        $all     = Point::where('uid', $request['uid'])->orderBy('created_at', 'desc')->get();
        $page    = Input::get('page', 1);
        $perPage = (int)getenv('PAGER');

        if ($all->count() === 0) {
            return redirect()->back();
        }

        $data = new LengthAwarePaginator(
            $all->forPage($page, $perPage),
            $all->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            [
                'path'  => $request->url(),
                'query' => $request->query()
            ]
        );

        return view('admin.users.point-list', ['points' => $data]);
    }

    public function user($uid)
    {
        $user = User::find($uid);
        return view('user.detail', ['user' => $user]);
    }

}
