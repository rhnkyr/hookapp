<?php

namespace App\Listeners;

use App\Enums\PromotionType;
use App\Events\ActiveChecker;
use App\Helper\CustomHelper;
use App\Models\Point;
use App\Models\Promotion;
use App\Models\UserPromotion;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SetUserLastActiveDate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActiveChecker $event
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle(ActiveChecker $event)
    {
        $user                   = $event->user;
        $user->last_action_date = Carbon::now();
        $user->save();
    }

}
