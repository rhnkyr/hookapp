<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 03 Mar 2018 18:42:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Achievement
 * 
 * @property int $id
 * @property string $receive_clap
 * @property string $give_clap
 * @property string $create_goal:
 * @property string $accomplish_goal
 * @property string $total_investment
 * @property string $total_reward
 * @property string $story_content
 * @property string $invite_x_friends
 * @property string $invest_more_than_x_coin
 * @property string $accomplishment
 * @property string $badge_claim
 *
 * @package App\Models
 */
class Achievement extends Eloquent
{
	public $timestamps = false;

    protected $hidden = [
        'id'
    ];

	protected $fillable = [
		'receive_clap',
		'give_clap',
		'create_goal:',
		'accomplish_goal',
		'total_investment',
		'total_reward',
		'story_content',
		'invite_x_friends',
		'invest_more_than_x_coin',
		'accomplishment',
		'badge_claim'
	];
}
