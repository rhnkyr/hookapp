<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 15:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FormDatum
 * 
 * @property int $id
 * @property string $email
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class FormData extends Eloquent
{
	protected $fillable = [
		'email',
		'message'
	];
}
