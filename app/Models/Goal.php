<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:14:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Goal
 *
 * @property int $id
 * @property string $gid
 * @property string $uid
 * @property string $title
 * @property int $investment
 * @property int $claps
 * @property int $up_votes
 * @property int $down_votes
 * @property int $status
 * @property \Carbon\Carbon $dead_line
 * @property \Carbon\Carbon $vote_finish
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Goal extends Eloquent
{
    protected $casts = [
        'investment' => 'int',
        'claps'      => 'int',
        'up_votes'   => 'int',
        'down_votes' => 'int',
        'status'     => 'int'
    ];

    protected $dates = [
        'dead_line'
    ];

    protected $fillable = [
        'gid',
        'uid',
        'title',
        'investment',
        'claps',
        'up_votes',
        'down_votes',
        'status',
        'dead_line',
        'vote_finish',
        'status'
    ];

    protected $hidden = [
        'id',
        'vote_finish',
        'updated_at',
    ];

    public function media()
    {
        return $this->hasMany(GoalMedia::class, 'gid', 'gid');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid');
    }
}
