<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:46:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GoalMedia
 *
 * @property int $id
 * @property string $gid
 * @property string $media_url
 * @property int $media_type
 * @property string $video_thumb
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class GoalMedia extends Eloquent
{
    protected $casts = [
        'media_type' => 'int',
        'is_active'  => 'int'
    ];

    protected $fillable = [
        'gid',
        'media_url',
        'media_type',
        'video_thumb',
        'is_active'
    ];

    protected $hidden = [
        'id',
        'is_active',
        'updated_at',
    ];

    public function goal()
    {
        return $this->hasOne(Goal::class, 'gid', 'gid');
    }
}
