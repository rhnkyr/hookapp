<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:14:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Point
 * 
 * @property int $id
 * @property string $uid
 * @property string $gid
 * @property int $amount
 * @property int $claim_type
 * @property \Carbon\Carbon $claim_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Point extends Eloquent
{
	protected $casts = [
		'amount' => 'int',
		'claim_type' => 'int'
	];

	protected $dates = [
		'claim_date'
	];

	protected $fillable = [
		'uid',
		'gid',
		'amount',
		'claim_type',
		'claim_date'
	];

    protected $hidden = [
        'id',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid');
    }
}
