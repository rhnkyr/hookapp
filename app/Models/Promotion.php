<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:14:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Promotion
 *
 * @property int $id
 * @property string $name
 * @property int $limit_point
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Promotion extends Eloquent
{
    protected $casts = [
        'limit_point' => 'int',
        'is_active'   => 'int'
    ];

    protected $fillable = [
        'name',
        'limit_point',
        'is_active'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'is_active'
    ];
}
