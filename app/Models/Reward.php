<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 16:43:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Reward
 * 
 * @property int $id
 * @property string $uid
 * @property string $gid
 * @property float $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Reward extends Eloquent
{
	protected $casts = [
		'amount' => 'float'
	];

	protected $fillable = [
		'uid',
		'gid',
		'amount'
	];
}
