<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 15:12:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Subscription
 * 
 * @property int $id
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Subscription extends Eloquent
{
	protected $fillable = [
		'email'
	];
}
