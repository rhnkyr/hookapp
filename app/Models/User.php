<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:14:53 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @property int $id
 * @property string $uid
 * @property string $username
 * @property string $password
 * @property string $profile_picture
 * @property string $email
 * @property string $remember_token
 * @property string $oss
 * @property string $ref_code
 * @property int $is_active
 * @property int $multiplier
 * @property string $one_signal_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $last_action_date
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $casts = [
        'is_active' => 'int',
        'role'      => 'int'
    ];

    protected $hidden = [
        'id',
        'password',
        'remember_token',
        'role',
        'os',
        'updated_at',
        'last_action_date',
        'is_active',
        'one_signal_id'
    ];

    protected $fillable = [
        'uid',
        'username',
        'password',
        'profile_picture',
        'email',
        'remember_token',
        'is_active',
        'role',
        'os',
        'multiplier',
        'last_action_date',
        'one_signal_id',
        'ref_code',
    ];

    public function goals()
    {
        return $this->hasMany(Goal::class, 'uid', 'uid');
    }

    public function points()
    {
        return $this->hasMany(Point::class, 'uid', 'uid');
    }

    public function promotions()
    {
        return $this->hasMany(UserPromotion::class, 'uid', 'uid');
    }

    public function achievements()
    {
        return $this->hasMany(UserAchievement::class, 'uid', 'uid');
    }

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Returns full img path of profile picture mutator
     * @return string
     */
    public function getProfileImagePathAttribute(): string
    {
        if ($this->profile_picture !== null || $this->profile_picture !== '') {

            return "/{$this->uid}/{$this->profile_picture}";
        }

        return '';
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
