<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Mar 2018 13:37:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAchievement
 * 
 * @property int $id
 * @property string $uid
 * @property int $receive_clap_rank
 * @property int $receive_clap_rank_claim
 * @property string $receive_clap_rank_notified
 * @property int $give_clap_rank
 * @property string $give_clap_rank_notified
 * @property int $give_clap_rank_claim
 * @property int $create_goal_rank
 * @property string $create_goal_rank_notified
 * @property int $create_goal_rank_claim
 * @property int $accomplish_goal_rank
 * @property string $accomplish_goal_rank_notified
 * @property int $accomplish_goal_rank_claim
 * @property int $total_investment_rank
 * @property string $total_investment_rank_notified
 * @property int $total_investment_rank_claim
 * @property int $total_reward_rank
 * @property string $total_reward_rank_notified
 * @property int $total_reward_rank_claim
 * @property int $story_content_rank
 * @property string $story_content_rank_notified
 * @property int $story_content_rank_claim
 * @property int $invite_x_friends_rank
 * @property string $invite_x_friends_rank_notified
 * @property int $invite_x_friends_rank_claim
 * @property int $invest_more_than_x_coin_rank
 * @property string $invest_more_than_x_coin_rank_notified
 * @property int $invest_more_than_x_coin_rank_claim
 * @property int $accomplishment_rank
 * @property string $accomplishment_rank_notified
 * @property int $accomplishment_rank_claim
 * @property int $badge_claim_rank
 * @property string $badge_claim_rank_notified
 * @property int $badge_claim_rank_claim
 *
 * @package App\Models
 */
class UserAchievement extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'receive_clap_rank' => 'int',
		'receive_clap_rank_claim' => 'int',
		'give_clap_rank' => 'int',
		'give_clap_rank_claim' => 'int',
		'create_goal_rank' => 'int',
		'create_goal_rank_claim' => 'int',
		'accomplish_goal_rank' => 'int',
		'accomplish_goal_rank_claim' => 'int',
		'total_investment_rank' => 'int',
		'total_investment_rank_claim' => 'int',
		'total_reward_rank' => 'int',
		'total_reward_rank_claim' => 'int',
		'story_content_rank' => 'int',
		'story_content_rank_claim' => 'int',
		'invite_x_friends_rank' => 'int',
		'invite_x_friends_rank_claim' => 'int',
		'invest_more_than_x_coin_rank' => 'int',
		'invest_more_than_x_coin_rank_claim' => 'int',
		'accomplishment_rank' => 'int',
		'accomplishment_rank_claim' => 'int',
		'badge_claim_rank' => 'int',
		'badge_claim_rank_claim' => 'int'
	];

	protected $fillable = [
		'uid',
		'receive_clap_rank',
		'receive_clap_rank_claim',
		'receive_clap_rank_notified',
		'give_clap_rank',
		'give_clap_rank_notified',
		'give_clap_rank_claim',
		'create_goal_rank',
		'create_goal_rank_notified',
		'create_goal_rank_claim',
		'accomplish_goal_rank',
		'accomplish_goal_rank_notified',
		'accomplish_goal_rank_claim',
		'total_investment_rank',
		'total_investment_rank_notified',
		'total_investment_rank_claim',
		'total_reward_rank',
		'total_reward_rank_notified',
		'total_reward_rank_claim',
		'story_content_rank',
		'story_content_rank_notified',
		'story_content_rank_claim',
		'invite_x_friends_rank',
		'invite_x_friends_rank_notified',
		'invite_x_friends_rank_claim',
		'invest_more_than_x_coin_rank',
		'invest_more_than_x_coin_rank_notified',
		'invest_more_than_x_coin_rank_claim',
		'accomplishment_rank',
		'accomplishment_rank_notified',
		'accomplishment_rank_claim',
		'badge_claim_rank',
		'badge_claim_rank_notified',
		'badge_claim_rank_claim'
	];

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid');
    }
}
