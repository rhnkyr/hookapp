<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 03 Mar 2018 19:05:56 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAchievementsTracker
 *
 * @property int $id
 * @property string $uid
 * @property int $receive_clap
 * @property int $give_clap
 * @property int $create_goal
 * @property int $accomplish_goal
 * @property int $total_investment
 * @property int $total_reward
 * @property int $story_content
 * @property int $invite_x_friends
 * @property int $invest_more_than_x_coin
 * @property int $accomplishment
 * @property int $badge_claim
 *
 * @package App\Models
 */
class UserAchievementsTracker extends Eloquent
{
    public $timestamps = false;

    protected $table = 'user_achievements_tracker';

    protected $hidden = [
        'id',
        'uid'
    ];

    protected $casts = [
        'receive_clap'            => 'int',
        'give_clap'               => 'int',
        'create_goal'             => 'int',
        'accomplish_goal'         => 'int',
        'total_investment'        => 'int',
        'total_reward'            => 'int',
        'story_content'           => 'int',
        'invite_x_friends'        => 'int',
        'invest_more_than_x_coin' => 'int',
        'accomplishment'          => 'int',
        'badge_claim'             => 'int'
    ];

    protected $fillable = [
        'uid',
        'receive_clap',
        'give_clap',
        'create_goal',
        'accomplish_goal',
        'total_investment',
        'total_reward',
        'story_content',
        'invite_x_friends',
        'invest_more_than_x_coin',
        'accomplishment',
        'badge_claim'
    ];
}
