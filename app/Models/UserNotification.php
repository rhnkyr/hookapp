<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 15 Mar 2018 17:56:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserNotification
 *
 * @property int $id
 * @property string $uid
 * @property string $url
 * @property string $text
 * @property int $is_read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserNotification extends Eloquent
{
    protected $casts = [
        'is_read' => 'int'
    ];

    protected $fillable = [
        'uid',
        'url',
        'text',
        'is_read'
    ];

    public function scopeUser($builder, $uid)
    {
        return $builder->where('uid', $uid);
    }

    public function scopeRead($builder, $type)
    {
        return $builder->where('is_read', $type);
    }
}
