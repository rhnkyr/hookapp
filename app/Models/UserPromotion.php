<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Feb 2018 14:14:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserPromotion
 *
 * @property int $id
 * @property string $uid
 * @property int $pid
 * @property int $notified
 * @property \Carbon\Carbon $claim_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserPromotion extends Eloquent
{
    protected $casts = [
        'pid'      => 'int',
        'notified' => 'int'
    ];

    protected $dates = [
        'claim_date'
    ];

    protected $fillable = [
        'uid',
        'pid',
        'claim_date',
        'notified'
    ];

    protected $hidden = [
        'id',
        'status',
        'updated_at',
    ];
}
