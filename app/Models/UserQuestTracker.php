<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 04 Mar 2018 09:38:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserQuestTracker
 * 
 * @property int $id
 * @property string $uid
 * @property int $complete_profile
 * @property int $first_share
 * @property int $weekly_top_1
 * @property int $weekly_top_3
 * @property int $weekly_top_5
 * @property int $weekly_top_10
 * @property int $monthly_top_1
 * @property int $monthly_top_3
 * @property int $monthly_top_5
 * @property int $monthly_top_10
 * @property int $yearly_top_1
 * @property int $yearly_top_3
 * @property int $yearly_top_5
 * @property int $yearly_top_10
 * @property int $complete_profile_notified
 * @property int $first_share_notified
 * @property int $weekly_top_1_notified
 * @property int $weekly_top_3_notified
 * @property int $weekly_top_5_notified
 * @property int $weekly_top_10_notified
 * @property int $monthly_top_1_notified
 * @property int $monthly_top_3_notified
 * @property int $monthly_top_5_notified
 * @property int $monthly_top_10_notified
 * @property int $yearly_top_1_notified
 * @property int $yearly_top_3_notified
 * @property int $yearly_top_5_notified
 * @property int $yearly_top_10_notified
 *
 * @package App\Models
 */
class UserQuestTracker extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'complete_profile' => 'int',
		'first_share' => 'int',
		'weekly_top_1' => 'int',
		'weekly_top_3' => 'int',
		'weekly_top_5' => 'int',
		'weekly_top_10' => 'int',
		'monthly_top_1' => 'int',
		'monthly_top_3' => 'int',
		'monthly_top_5' => 'int',
		'monthly_top_10' => 'int',
		'yearly_top_1' => 'int',
		'yearly_top_3' => 'int',
		'yearly_top_5' => 'int',
		'yearly_top_10' => 'int',
		'complete_profile_notified' => 'int',
		'first_share_notified' => 'int',
		'weekly_top_1_notified' => 'int',
		'weekly_top_3_notified' => 'int',
		'weekly_top_5_notified' => 'int',
		'weekly_top_10_notified' => 'int',
		'monthly_top_1_notified' => 'int',
		'monthly_top_3_notified' => 'int',
		'monthly_top_5_notified' => 'int',
		'monthly_top_10_notified' => 'int',
		'yearly_top_1_notified' => 'int',
		'yearly_top_3_notified' => 'int',
		'yearly_top_5_notified' => 'int',
		'yearly_top_10_notified' => 'int'
	];

	protected $fillable = [
		'uid',
		'complete_profile',
		'first_share',
		'weekly_top_1',
		'weekly_top_3',
		'weekly_top_5',
		'weekly_top_10',
		'monthly_top_1',
		'monthly_top_3',
		'monthly_top_5',
		'monthly_top_10',
		'yearly_top_1',
		'yearly_top_3',
		'yearly_top_5',
		'yearly_top_10',
		'complete_profile_notified',
		'first_share_notified',
		'weekly_top_1_notified',
		'weekly_top_3_notified',
		'weekly_top_5_notified',
		'weekly_top_10_notified',
		'monthly_top_1_notified',
		'monthly_top_3_notified',
		'monthly_top_5_notified',
		'monthly_top_10_notified',
		'yearly_top_1_notified',
		'yearly_top_3_notified',
		'yearly_top_5_notified',
		'yearly_top_10_notified'
	];
}
