<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 15 Feb 2018 13:44:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserClap
 * 
 * @property int $id
 * @property string $gid
 * @property string $uid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserVote extends Eloquent
{
	protected $fillable = [
		'gid',
		'uid',
	];

    protected $hidden = [
        'id',
        'updated_at',
    ];
}
