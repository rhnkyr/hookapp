<?php

return [

    'ios' => [
        'environment' => env('IOS_PUSH_ENV', 'development'),
        'certificate' => app_path() . '/myCert.pem',
        'passPhrase'  => env('IOS_PUSH_PASSWORD', '291923Job'),
        'service'     => 'apns'
    ],

    'android' => [
        'environment' => env('ANDROID_PUSH_ENV', 'development'),
        'apiKey'      => env('ANDROID_PUSH_API_KEY', 'yourAPIKey'),
        'service'     => 'gcm'
    ]

];