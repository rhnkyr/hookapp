<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 50);
            $table->string('gid', 50);
            $table->tinyInteger('type', false, true)->nullable();
            $table->integer('amount', false, true)->nullable();
            $table->tinyInteger('status', false, true)->nullable();
            $table->timestamps();

            $table->index('uid');
            $table->index('gid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
