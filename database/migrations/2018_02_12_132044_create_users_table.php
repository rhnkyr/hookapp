<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 50);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('profile_picture', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->string('device_id', 50)->nullable();
            $table->string('os', 10)->nullable();
            $table->string('one_signal_id', 10)->nullable();
            $table->string('ref_code', 10)->nullable();
            $table->tinyInteger('multiplier', false, true)->default(0);
            $table->tinyInteger('is_active', false, true)->default(0);
            $table->tinyInteger('role', false, true)->default(0);
            $table->dateTime('last_action_date')->default(\Carbon\Carbon::now());
            $table->timestamps();

            $table->index('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
