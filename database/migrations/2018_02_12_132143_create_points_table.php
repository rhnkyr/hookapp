<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 50);
            $table->string('gid', 50);
            $table->string('hid', 50);
            $table->integer('amount', false, true)->nullable();
            $table->tinyInteger('claim_type', false, true)->nullable();
            $table->dateTime('claim_date')->nullable();
            $table->timestamps();

            $table->index('uid');
            $table->index('gid');
            $table->index('hid');
            $table->index(['uid', 'claim_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
