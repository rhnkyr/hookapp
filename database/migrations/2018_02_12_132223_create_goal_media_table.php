<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gmid', 50);
            $table->string('gid', 50);
            $table->string('media_url', 255);
            $table->tinyInteger('media_type', false, true)->default(0);
            $table->tinyInteger('is_active', false, true)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_media');
    }
}
