<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gid',50);
            $table->string('uid',50);
            $table->string('title',255);
            $table->integer('investment',false,true);
            $table->integer('claps',false,true)->default(0);
            $table->integer('up_votes',false,true)->default(0);
            $table->integer('down_votes',false,true)->default(0);
            $table->tinyInteger('status',false,true)->default(0);
            $table->dateTime('dead_line');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goals');
    }
}
