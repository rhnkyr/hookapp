<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //'failed' => 'These credentials do not match our records.',
    'main_title'  => 'Hook Admin Login',
    'user_name'   => 'Username',
    'password'    => 'Password',
    'sign_in_btn' => 'Sign In',
    'error'       => 'Please check your credentials and try again',
    'throttle'    => 'Too many login attempts. Please try again in :seconds seconds.',

];
