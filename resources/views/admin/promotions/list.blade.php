@extends('_layout.default')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Promotion List</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('admin.welcome')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Promotions</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">

                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Name</th>
                                <th> Limit Point</th>
                                <th> Multiplier</th>
                                <th> Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($promotions as $promotion)
                                <tr>
                                    <td>{{$promotion->name}}</td>
                                    <td> {{number_format($promotion->limit_point,0,',','.')}} </td>
                                    <td> {{$promotion->multiplier}} </td>
                                    <td class="text-center">
                                        @if($promotion->is_active === 1)
                                            <span class="label label-sm label-success"> Active </span>
                                        @else
                                            <span class="label label-sm label-warning"> Passive </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BREADCRUMB -->
@endsection