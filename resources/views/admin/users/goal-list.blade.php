@extends('_layout.default')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>User's Goal List</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('admin.welcome')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">User's Goals</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-backward font-dark"></i>
                        <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Title</th>
                                <th> Investment</th>
                                <th> Total Clap</th>
                                <th> Total Up Vote</th>
                                <th> Total Down Vote</th>
                                <th> Created At</th>
                                <th> Dead Line</th>
                                <th> Total Media</th>
                                <th> Status</th>
                                <th> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($goals as $goal)
                                <tr>
                                    <td> {{$goal->title}} </td>
                                    <td class="text-center"> {{$goal->investment}} </td>
                                    <td class="text-center"> {{$goal->claps}} </td>
                                    <td class="text-center"> {{$goal->up_votes}} </td>
                                    <td class="text-center"> {{$goal->down_votes}} </td>
                                    <td class="text-center"> {{date('d-m-Y H:i:s', strtotime($goal->created_at))}} </td>
                                    <td class="text-center"> {{date('d-m-Y H:i:s', strtotime($goal->dead_line))}} </td>
                                    <td class="text-center"> {{$goal->total_media}} </td>
                                    <td class="text-center">
                                        @if($goal->status === 1)
                                            <span class="label label-sm label-success"> Active </span>
                                        @else
                                            <span class="label label-sm label-warning"> Passive </span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{route('admin.user.goal_media', ['gid'=>$goal->gid])}}" class="label label-sm label-success "> Show Media </a>
                                        <!--<a href="{{route('admin.user.point_list', ['uid'=>$goal->gid])}}" class="label label-sm label-info m-l-5"> Points </a>
                                        <a href="{{route('admin.user.coin_list', ['uid'=>$goal->gid])}}" class="label label-sm label-warning m-l-5"> Coins </a>-->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="pull-right">
                                {{ $goals->links("pagination::bootstrap-4") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BREADCRUMB -->
@endsection