@extends('_layout.default')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{$medias[0]->goal->title}} / Goal Media List</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('admin.welcome')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Goal Media</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-backward font-dark"></i>
                        <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Media</th>
                                <th> Type</th>
                                <th> Created At</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($medias as $media)
                                <tr>
                                    <td class="text-center">
                                        @if($media->media_type === 1)
                                            <img src="{{$upload_path}}/{{$media->goal->uid}}/{{$media->media_url}}" style="width: 300px"/>
                                        @else
                                            <video width="320" height="240" controls>
                                                <source src="{{$upload_path}}/{{$media->goal->uid}}/{{$media->media_url}}" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($media->media_type === 1)
                                            <span class="label label-sm label-success"> Photo </span>
                                        @else
                                            <span class="label label-sm label-info"> Video </span>
                                        @endif
                                    </td>
                                    <td class="text-center"> {{date('d-m-Y H:i:s', strtotime($media->created_at))}} </td>
                                    <!--<td class="text-center">
                                        @if($media->status === 1)
                                            <span class="label label-sm label-success"> Active </span>
                                        @else
                                            <span class="label label-sm label-warning"> Passive </span>
                                        @endif
                                    </td>-->
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BREADCRUMB -->
@endsection