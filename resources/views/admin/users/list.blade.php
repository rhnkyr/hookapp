@extends('_layout.default')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>User List</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('admin.welcome')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Users</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">

                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Profile Picture</th>
                                <th> User Name</th>
                                <th> Email</th>
                                <th> OS</th>
                                <th> Multiplier</th>
                                <th> Total Goals</th>
                                <th> Created At</th>
                                <th> Status</th>
                                <th> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    @if($user->profile_picture)
                                    <td class="text-center"><img src="{{$upload_path}}/{{$user->uid}}/{{$user->profile_picture}}" style="width: 100px"/></td>
                                    @else
                                    <td class="text-center">--</td>
                                    @endif
                                    <td> {{$user->username}} </td>
                                    <td> {{$user->email}} </td>
                                    <td class="text-center"> {{$user->os}} </td>
                                    <td class="text-center"> {{$user->multiplier}} </td>
                                    <td class="text-center"> {{$user->total_goals}} </td>
                                    <td class="text-center"> {{date('d-m-Y H:i:s', strtotime($user->created_at))}} </td>
                                    <td class="text-center">
                                        @if($user->is_active === 1)
                                            <span class="label label-sm label-success"> Active </span>
                                        @else
                                            <span class="label label-sm label-warning"> Passive </span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{route('admin.user.goal_list', ['uid'=>$user->uid])}}" class="label label-sm label-success "> Goals </a>
                                        <a href="{{route('admin.user.point_list', ['uid'=>$user->uid])}}" class="label label-sm label-info m-l-5"> Points </a>
                                        <a href="{{route('admin.user.coin_list', ['uid'=>$user->uid])}}" class="label label-sm label-warning m-l-5"> Coins </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="pull-right">
                                {{ $users->links("pagination::bootstrap-4") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BREADCRUMB -->
@endsection