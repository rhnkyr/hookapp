@extends('_layout.default')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>User's Point List</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('admin.welcome')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">User's Points</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-backward font-dark"></i>
                        <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Amount</th>
                                <th> Type</th>
                                <th> Claim Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($points as $point)
                                <tr>
                                    <td class="text-center"> {{$point->amount}} </td>
                                    <td class="text-center"> {{  \App\Enums\PointType::getDescription($point->claim_type)  }} </td>
                                    <td class="text-center"> {{date('d-m-Y H:i:s', strtotime($point->claim_date))}} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="pull-right">
                                {{ $points->links("pagination::bootstrap-4") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BREADCRUMB -->
@endsection