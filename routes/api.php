<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth', 'namespace' => 'App\Api\V1\Controllers'], function (Router $api) {

        $api->post('signup', 'SignUpController@signUp');
        $api->post('login', 'LoginController@login');
        $api->post('logout', 'LogoutController@logout');
        //$api->post('refresh', 'RefreshController@refresh');
    });

    $api->group(['middleware' => 'jwt.auth', 'prefix' => 'user', 'namespace' => 'App\Api\V1\Controllers'], function (Router $api) {

        $api->get('achievements', 'UserController@achievements');
        $api->get('me', 'UserController@me');
        $api->get('non-read-notifications', 'NotificationController@nonReadNotifications');
        $api->get('read-notifications', 'NotificationController@readNotifications');
        $api->get('seen-notifications', 'NotificationController@seen');
        $api->get('{uid}', 'UserController@user');

        $api->post('edit', 'UserController@edit');
        $api->post('claim', 'UserController@claim');

        $api->group(['prefix' => 'goal'], function (Router $api) {

            $api->get('list', 'GoalController@goalList');
            $api->get('{gid}', 'GoalController@detail');
            $api->get('{gid}/clap', 'GoalController@clap');
            $api->get('{gid}/up', 'GoalController@upVote');
            $api->get('{gid}/down', 'GoalController@downVote');
            $api->get('completed/{page}', 'GoalController@completedGoals');
            $api->post('create', 'GoalController@create');
            //$api->post('update', 'GoalController@update');
            //$api->post('delete', 'GoalController@delete');
            $api->post('media', 'MediaController@create');
            $api->post('media/delete', 'MediaController@delete');

        });

    });

    $api->group(['middleware' => 'jwt.auth', 'prefix' => 'leaderboard', 'namespace' => 'App\Api\V1\Controllers'], function (Router $api) {

        $api->get('weekly', 'LeaderBoardController@weekly');
        $api->get('monthly', 'LeaderBoardController@monthly');
        $api->get('all-time', 'LeaderBoardController@all');

    });

    $api->group(['middleware' => 'jwt.auth', 'prefix' => 'data', 'namespace' => 'App\Api\V1\Controllers'], function (Router $api) {
        $api->get('grades', 'UserController@promotions');
    });


    $api->get('check-username/{username}', 'App\Api\V1\Controllers\SignUpController@checkUsername');
    $api->get('check-email/{email}', 'App\Api\V1\Controllers\SignUpController@checkEmail');
    $api->get('check-refcode/{ref_code}', 'App\Api\V1\Controllers\SignUpController@checkRefCode');
    $api->get('achievements-list', 'App\Api\V1\Controllers\SettingsController@achievements');
    $api->post('subs','App\Api\V1\Controllers\SiteController@subscription');
    $api->post('contact','App\Api\V1\Controllers\SiteController@contact');

    $api->get('/', function () {
        return response()->json([
            'message' => 'Hook App API'
        ]);
    });
});
