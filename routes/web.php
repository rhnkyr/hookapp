<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('reset_password/{token}', ['as' => 'password.reset', function ($token) {
    // implement your reset password route here!
}]);*/

Route::group(['prefix' => 'back-office'], function () {

    Route::get('/dashboard', 'Panel\CommonController@index')->name('admin.welcome');

    Route::group(['prefix' => 'users'], function () {
        Route::get('/user/{uid}', 'Panel\UserController@user')->name('admin.user');
        Route::get('/goal-list/{uid}', 'Panel\UserController@goalList')->name('admin.user.goal_list');
        Route::get('/goal-media/{gid}', 'Panel\UserController@goalMediaList')->name('admin.user.goal_media');
        Route::get('/point-list/{uid}', 'Panel\UserController@pointList')->name('admin.user.point_list');
        Route::get('/coin-list/{uid}', 'Panel\UserController@coinList')->name('admin.user.coin_list');
        Route::get('/list', 'Panel\UserController@userList')->name('admin.user.list');
    });

    Route::group(['prefix' => 'promotions'], function () {
        Route::get('/promotion/{pid?}', 'Panel\PromotionController@promotion')->name('admin.promotion');
        Route::get('/list', 'Panel\PromotionController@promotionList')->name('admin.promotion.list');
        Route::post('/save', 'Panel\PromotionController@save')->name('admin.promotion.save');
        Route::post('/update', 'Panel\PromotionController@update')->name('admin.promotion.update');
        Route::post('/delete', 'Panel\PromotionController@delete')->name('admin.promotion.delete');
    });

    Route::group(['prefix' => 'goals'], function () {
        Route::get('/goal/{gid}', 'Panel\GoalController@goal')->name('admin.goal');
        Route::get('/list', 'Panel\GoalController@goalList')->name('admin.goal.list');
        Route::post('/delete', 'Panel\GoalController@delete')->name('admin.goal.delete');
    });

    Route::group(['prefix' => 'points'], function () {
        Route::get('/list', 'Panel\PointController@pointList')->name('admin.point.list');
        Route::get('/weekly-list', 'Panel\PointController@weekList')->name('admin.point.week.list');
        Route::get('/monthly-list', 'Panel\PointController@monthList')->name('admin.point.month.list');
        Route::get('/all-time-list', 'Panel\PointController@allTimeList')->name('admin.point.alltime.list');
    });

    Route::group(['prefix' => 'coins'], function () {
        Route::get('/list', 'Panel\CoinController@pointList')->name('admin.coin.list');
        Route::get('/weekly-list', 'Panel\CoinController@weekList')->name('admin.coin.week.list');
        Route::get('/monthly-list', 'Panel\CoinController@monthList')->name('admin.coin.month.list');
        Route::get('/all-time-list', 'Panel\CoinController@allTimeList')->name('admin.coin.alltime.list');
    });

    Route::get('/log-out', 'Panel\AuthController@logout')->name('user.logout');

});


Route::post('/check', 'Panel\AuthController@check')->name('user.check');
Route::get('/', 'Panel\AuthController@index')->name('index');
